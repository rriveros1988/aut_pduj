<?php
	ini_set('display_errors', 'Off');
	// header('Access-Control-Allow-Origin: *');
	require('consultas.php');
	require("PHPExcel.php");
  require("phpmailer/PHPMailerAutoload.php");
  include('simple_html_dom.php');
  date_default_timezone_set('America/Santiago');

  $row = consultaExcelEzentis();
	$datosExcel = array();
  $datosExcel[] = array('RUC','RIT', 'TRIBUNAL','FECHA_INGRESO','CARATULADO');

	if(is_array($row)){
    for($i = 0; $i < count($row); $i++){
        $datosExcel[] = array($row[$i]['RUC'],$row[$i]['RIT'],$row[$i]['TRIBUNAL'],$row[$i]['FECHA_INGRESO'],$row[$i]['CARATULADO']);
    }
	}

	$excel = new PHPExcel();

  // Fill worksheet from values in array
  $excel->getActiveSheet()->fromArray($datosExcel, null, 'A1');

  // Rename worksheet
  $excel->getActiveSheet()->setTitle("DEMANDAS_30_DIAS");

  // Set AutoSize for name and email fields
  $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
  $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
  $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
  $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
  $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

	$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
	$writer->save("Demandas_30_dias.xlsx");

  $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
  $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

  $fecha = strtotime('-0 day');
  $fecha = $dias[date('w', $fecha)]." ".date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " ".date('Y', $fecha) . " a las " .date("H:i:s") . " hrs.";

  $mail = new PHPMailer(); // defaults to using php "mail()"

  //Codificacion
  $mail->CharSet = 'UTF-8';

  //indico a la clase que use SMTP
  $mail->IsSMTP();
  //Debo de hacer autenticación SMTP
  $mail->SMTPAuth = true;
  //indico el servidor SMTP
  $mail->Host = "correo.ezentis.cl";
  //indico el puerto que usa Gmail
  $mail->Port = 25;
  //indico un usuario / clave de un usuario
  $mail->Username = "cgo";
  $mail->Password = "DifhojTK";

  $mail->SMTPOptions = array(
      'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
      )
  );

   $firma = file_get_html("http://operaciones.ezentis.cl/logos/firma.php");

   $body = "
         <div style='width: 100%; text-align: justify; margin: 0 auto;'>
         <font style='font-size: 14px;'>
         Estimado/a,
         <br />
         <br />
         Adjuntamos información de las demandas laborales en los últimos 30 días.
         <br />
         <br />
         </div>
         <div'>
             <font style='font-size: 14px;'>
                 Saludos cordiales.
             </font>
             <br />
             " . $firma . "
         </div>
         ";

 $mail->SetFrom('cgo@ezentis.cl', "CGO");

 //defino la dirección de email de "reply", a la que responder los mensajes
 //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
 $mail->AddReplyTo("cgo@ezentis.cl", "CGO");

  $mail->AddAddress('camila.retamales@ezentis.cl','camila.retamales@ezentis.cl');
  $mail->addBCC('rodrigo.riveros@ezentis.cl','rodrigo.riveros@ezentis.cl');

  $mail->AddAttachment("Demandas_30_dias.xlsx");

  $mail->Subject = "Demandas últimos 30 días Grupo Ezentis // " . $fecha;

  $mail->MsgHTML($body);

  if(!$mail->Send()) {
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
      echo 'Message has been sent';
  }
?>
