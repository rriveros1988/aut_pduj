<?php
  date_default_timezone_set('America/Santiago');
  ini_set('display_errors', 'On');
  set_time_limit(300);
  require('consultas.php');
  require('simple_html_dom.php');

  $cookie = "cookieTT.txt";
  // $cookie = "cookieTT.txt";

  $array = array(
    "FLG_Autoconsulta" => "1"
  );

  $url = "https://laboral.pjud.cl/SITLAPORWEB/InicioAplicacionPortal.do";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
  // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
  // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
  // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

  $respuesta = curl_exec($ch);

  curl_close($ch);

  $url2 = "https://laboral.pjud.cl/SITLAPORWEB/AtPublicoViewAccion.do?tipoMenuATP=1";

  $ch2 = curl_init();
  curl_setopt($ch2, CURLOPT_URL, $url2);
  curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch2, CURLOPT_HEADER, false);
  curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch2, CURLOPT_POST, false);
  // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
  // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
  // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
  // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

  $respuesta2 = curl_exec($ch2);

  curl_close($ch2);

  $hora = '';
  $TS01c64c27 = '';
  $TS01ca5743 = '';
  $JSESSIONID = '';
  $fp = fopen($cookie, "r");
  while (!feof($fp)){
      $linea = fgets($fp);
      // echo $linea ."<br>";
      if(strpos($linea,"HORA_LOGIN") > -1){
        $d = explode("HORA_LOGIN", $linea);
        // echo trim($d[1]) . "<br>";
        $hora = trim($d[1]);
      }
      if(strpos($linea,"TS01c64c27") > -1){
        $d = explode("TS01c64c27", $linea);
        // echo trim($d[1]) . "<br>";
        $TS01c64c27 = trim($d[1]);
      }
      if(strpos($linea,"TS01ca5743") > -1){
        $d = explode("TS01ca5743", $linea);
        // echo trim($d[1]) . "<br>";
        $TS01ca5743 = trim($d[1]);
      }
      if(strpos($linea,"JSESSIONID") > -1){
        $d = explode("JSESSIONID", $linea);
        // echo trim($d[1]) . "<br>";
        $JSESSIONID = trim($d[1]);
      }
  }
  fclose($fp);

  $array = array(
    "TIP_Consulta"=>"5",
    "TIP_Lengueta"=>"tdDos",
    "SeleccionL"=>"0",
    "TIP_Causa"=>"",
    "ROL_Causa"=>"",
    "ERA_Causa"=>"0",
    "RUC_Era"=>"",
    "RUC_Tribunal"=>"4",
    "RUC_Numero"=>"",
    "RUC_Dv"=>"",
    "FEC_Desde"=>"01/05/2018",
    "FEC_Hasta"=>"15/05/2018",
    "SEL_Trabajadores"=>"0",
    "RUT_Consulta"=>"",
    "RUT_DvConsulta"=>"",
    "irAccionAtPublico"=>"Consulta",
    "NOM_Consulta"=>"",
    "APE_Paterno"=>"",
    "APE_Materno"=>"",
    "GLS_Razon"=>"",
    "COD_Tribunal"=>"1333",
    "COD_TribunalSinTodos"=>"1333"
  );

  $request = array();
  $request[] = 'Host: laboral.pjud.cl';
  $request[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0';
  $request[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
  $request[] = 'Accept-Language: es-CL,es;q=0.8,en-US;q=0.5,en;q=0.3';
  $request[] = 'Accept-Encoding: gzip, deflate, br';
  $request[] = 'Content-Type: application/x-www-form-urlencoded';
  $request[] = 'Content-Length: 345';
  $request[] = 'DNT: 1';

  $url3 = "https://laboral.pjud.cl/SITLAPORWEB/AtPublicoDAction.do";

  $ch3 = curl_init();
  curl_setopt($ch3, CURLOPT_URL, $url3);
  curl_setopt($ch3, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  // curl_setopt($ch3, CURLOPT_HEADER, true);
  curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch3, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch3, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch3, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch3, CURLOPT_POST, true);
  // curl_setopt($ch3, CURLOPT_HTTPHEADER, $request);
  curl_setopt($ch3, CURLOPT_POSTFIELDS,http_build_query($array));
  // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
  // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
  // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

  $respuesta3 = curl_exec($ch3);

  curl_close($ch3);

  $html3 = new DOMDocument;
  $html3->loadHTML($respuesta3);
  foreach ($html3->getElementsByTagName('table') as $tag3) {
      if($tag3->getAttribute('id') === 'filaSel'){
        foreach ($tag3->getElementsByTagName('a') as $tagHijo3) {
          $url4 = "https://laboral.pjud.cl" . trim($tagHijo3->getAttribute('href'));

          $ch4 = curl_init();
          curl_setopt($ch4, CURLOPT_URL, $url4);
          curl_setopt($ch4, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
          curl_setopt($ch4, CURLOPT_HEADER, false);
          curl_setopt($ch4, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch4, CURLOPT_SSL_VERIFYPEER, true);
          curl_setopt($ch4, CURLOPT_COOKIEFILE, $cookie);
          curl_setopt($ch4, CURLOPT_COOKIEJAR, $cookie);
          curl_setopt($ch4, CURLOPT_POST, false);
          // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
          // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
          // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
          // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
          // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

          $respuesta4 = curl_exec($ch4);

          $arch = fopen("/var/www/html/Git/aut_pduj/test4.html", "w+");
          fwrite($arch, $respuesta4);
          fclose($arch);

          $datos_final = array();

          $html4 = new DOMDocument;
          $html4->loadHTML($respuesta4);
          foreach ($html4->getElementsByTagName('td') as $tag4) {
            if($tag4->getAttribute('bgcolor') === '#e0e9f0'){
              $elemento = explode(":", $tag4->textContent);
              $elemento[0] = trim($elemento[0]);
              $elemento[1] = trim($elemento[1]);
              $datos_final[] = $elemento;
            }
          }

          unset($datos_final[10]);
          unset($datos_final[11]);
          unset($datos_final[12]);
          unset($datos_final[13]);

          $datos_final[1][1] = $datos_final[1][0];
          $datos_final[1][0] = "Caratulado";

          echo "<pre>";
          var_dump($datos_final);
          echo "</pre>";

          $fecha = date('Y-m-d', strtotime(str_replace("/","-",$datos_final[2][1])));

          $cl = ingresaCL($datos_final[0][1],$datos_final[1][1],$fecha,$datos_final[3][1],$datos_final[4][1],$datos_final[5][1],$datos_final[6][1],$datos_final[7][1],$datos_final[8][1],$datos_final[9][1]);

          echo $cl . "<br>";

          echo "---------------------------<br>";

          $datos_litigantes = array();

          $html5 = new DOMDocument;
          $html5->loadHTML($respuesta4);
          foreach ($html5->getElementsByTagName('div') as $tag5) {
            if($tag5->getAttribute('id') === 'Litigantes'){
              foreach ($tag5->getElementsByTagName('table') as $tag6) {
                if($tag6->getAttribute('class') === 'textoPortal'){
                  $elem_lim = array();
                  $a = 0;
                  foreach ($tag6->getElementsByTagName('td') as $tag6) {
                    $a++;
                    $elem_lim[] = trim($tag6->textContent);
                    if($a === 6){
                      $datos_litigantes[] = $elem_lim;
                      $elem_lim = array();
                      $a = 0;
                    }
                  }

                }
              }
            }
          }

          // echo "<pre>";
          // var_dump($datos_litigantes);
          // echo "</pre>";

          if($cl === "Ok"){
            for($i = 0; $i < count($datos_litigantes); $i++){
              $b = ingresaCL_Lit($datos_final[3][1],$datos_litigantes[$i][1],$datos_litigantes[$i][2],$datos_litigantes[$i][3],$datos_litigantes[$i][4],$datos_litigantes[$i][5],$datos_final[0][1],$datos_final[9][1]);
              echo $b . "<br>";
            }
          }

          echo "---------------------------<br>";

          $datos_materias = array();

          $html6 = new DOMDocument;
          $html6->loadHTML($respuesta4);
          foreach ($html6->getElementsByTagName('div') as $tag5) {
            if($tag5->getAttribute('id') === 'Materias'){
              foreach ($tag5->getElementsByTagName('table') as $tag6) {
                if($tag6->getAttribute('bordercolor') === '#def0fe'){
                  $elem_lim = array();
                  $a = 0;
                  foreach ($tag6->getElementsByTagName('td') as $tag6) {
                    if(trim($tag6->textContent) !== ''){
                      $a++;
                      $elem_lim[] = trim($tag6->textContent);
                    }
                    if($a === 4){
                      unset($elem_lim[2]);
                      unset($elem_lim[3]);
                      $datos_materias[] = $elem_lim;
                      $elem_lim = array();
                      $a = 0;
                    }
                  }
                }
              }
            }
          }

          // echo "<pre>";
          // var_dump($datos_materias);
          // echo "</pre>";

          if($cl === "Ok"){
            for($i = 0; $i < count($datos_materias); $i++){
              $c = ingresaCL_Mat($datos_final[3][1],$datos_materias[$i][0],$datos_materias[$i][1],$datos_final[0][1],$datos_final[9][1]);
              echo $c . "<br>";
            }
          }

          $datos_historia = array();

          $html6 = new DOMDocument;
          $html6->loadHTML($respuesta4);
          foreach ($html6->getElementsByTagName('div') as $tag5) {
            if($tag5->getAttribute('id') === 'Historia'){
              foreach ($tag5->getElementsByTagName('table') as $tag6) {
                if($tag6->getAttribute('class') === 'textoPortal'){
                  $elem_lim = array();
                  for($a = 0; $a < count($tag6->firstChild->childNodes); $a=$a+2){
                    if(trim($tag6->firstChild->childNodes[$a]->childNodes[6]->textContent) !== ""){
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[0]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[6]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[8]->textContent);
                      $dato3 = trim($tag6->firstChild->childNodes[$a]->childNodes[10]->textContent);
                      foreach ($tag6->firstChild->childNodes[$a]->childNodes[10]->getElementsByTagName('a') as $tag7) {
                        if($tag7->getAttribute('href') === '#'){
                          $dato3 = trim($tag7->textContent);
                        }
                      }
                      $elem_lim[] = $dato3;
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[12]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[14]->textContent);
                      echo "--------------------------<br>";

                      $datos_historia[] = $elem_lim;
                      $elem_lim = array();
                    }
                    else{
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[0]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[7]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[9]->textContent);
                      $dato3 = trim($tag6->firstChild->childNodes[$a]->childNodes[11]->textContent);
                      foreach ($tag6->firstChild->childNodes[$a]->childNodes[11]->getElementsByTagName('a') as $tag7) {
                        if($tag7->getAttribute('href') === '#'){
                          $dato3 = trim($tag7->textContent);
                        }
                      }
                      $elem_lim[] = $dato3;
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[13]->textContent);
                      $elem_lim[] = trim($tag6->firstChild->childNodes[$a]->childNodes[15]->textContent);
                      echo "--------------------------<br>";

                      $datos_historia[] = $elem_lim;
                      $elem_lim = array();
                    }
                  }
                }
              }
            }
          }

          // echo "<pre>";
          // var_dump($datos_historia);
          // echo "</pre>";

          if($cl === "Ok"){
            for($i = 0; $i < count($datos_historia); $i++){
              $fecha_Mov = date('Y-m-d', strtotime(str_replace("/","-",$datos_historia[$i][4])));
              $d = ingresaCL_Mov($datos_final[3][1],$datos_final[0][1],$datos_historia[$i][0],$datos_historia[$i][1],$datos_historia[$i][2],$datos_historia[$i][3],$fecha_Mov,$datos_historia[$i][5],$datos_final[9][1]);
              echo $d . "<br>";
            }
          }

          curl_close($ch4);

          // break;
        }
      }
  }
  // unlink($cookie);
?>
