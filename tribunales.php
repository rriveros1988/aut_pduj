<?php
  date_default_timezone_set('America/Santiago');
  // ini_set('display_errors', 'On');
  set_time_limit(3600);
  require('consultas.php');
  require('simple_html_dom.php');

  $cookie = "/var/www/html/Git/aut_pduj/cookieTT.txt";
  // $cookie = "cookieTT.txt";

  $array = array(
    "FLG_Autoconsulta" => "1"
  );

  $url = "https://laboral.pjud.cl/SITLAPORWEB/InicioAplicacionPortal.do";

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch, CURLOPT_HEADER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
  // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
  // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
  // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

  $respuesta = curl_exec($ch);

  curl_close($ch);

  $url2 = "https://laboral.pjud.cl/SITLAPORWEB/AtPublicoViewAccion.do?tipoMenuATP=1";

  $ch2 = curl_init();
  curl_setopt($ch2, CURLOPT_URL, $url2);
  curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0");
  curl_setopt($ch2, CURLOPT_HEADER, false);
  curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, false);
  curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch2, CURLOPT_POST, false);
  // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
  // curl_setopt($ch, CURLOPT_PROXY, '118.174.211.220');
  // curl_setopt($ch, CURLOPT_PROXYPORT, '11');
  // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");
  // curl_setopt($ch3, CURLOPT_AUTOREFERER, true);

  $respuesta2 = curl_exec($ch2);

  curl_close($ch2);

  $html2 = new DOMDocument;
  $html2->loadHTML($respuesta2);
  foreach ($html2->getElementsByTagName('select') as $tag2) {
    if($tag2->getAttribute('name') === 'COD_Tribunal'){
      foreach ($tag2->getElementsByTagName('option') as $tag2Hijo) {
        if(strlen(trim($tag2Hijo->getAttribute("value"))) > 1 && trim($tag2Hijo->getAttribute("value")) !== '0'){
          $arch = fopen("/var/www/html/Git/aut_pduj/tribunal.txt", "w+");
    	    fwrite($arch, trim($tag2Hijo->getAttribute("value")) . " - " . $tag2Hijo->textContent . "\n");
    	    fclose($arch);

          echo trim($tag2Hijo->getAttribute("value")) . " ";
          echo trim($tag2Hijo->nodeValue) . "<br>";

          ingresaTr(trim($tag2Hijo->getAttribute("value")),trim($tag2Hijo->nodeValue));

          $hora = '';
          $TS01c64c27 = '';
          $TS01ca5743 = '';
          $JSESSIONID = '';
          $fp = fopen($cookie, "r");
          while (!feof($fp)){
              $linea = fgets($fp);
              // echo $linea ."<br>";
              if(strpos($linea,"HORA_LOGIN") > -1){
                $d = explode("HORA_LOGIN", $linea);
                // echo trim($d[1]) . "<br>";
                $hora = trim($d[1]);
              }
              if(strpos($linea,"TS01c64c27") > -1){
                $d = explode("TS01c64c27", $linea);
                // echo trim($d[1]) . "<br>";
                $TS01c64c27 = trim($d[1]);
              }
              if(strpos($linea,"TS01ca5743") > -1){
                $d = explode("TS01ca5743", $linea);
                // echo trim($d[1]) . "<br>";
                $TS01ca5743 = trim($d[1]);
              }
              if(strpos($linea,"JSESSIONID") > -1){
                $d = explode("JSESSIONID", $linea);
                // echo trim($d[1]) . "<br>";
                $JSESSIONID = trim($d[1]);
              }
          }
          fclose($fp);

          $array = array(
            "TIP_Consulta"=>"5",
            "TIP_Lengueta"=>"tdDos",
            "SeleccionL"=>"0",
            "TIP_Causa"=>"",
            "ROL_Causa"=>"",
            "ERA_Causa"=>"0",
            "RUC_Era"=>"",
            "RUC_Tribunal"=>"4",
            "RUC_Numero"=>"",
            "RUC_Dv"=>"",
            "FEC_Desde"=>$_GET['fechaIni'],
            "FEC_Hasta"=>$_GET['fechaFin'],
            "SEL_Trabajadores"=>"0",
            "RUT_Consulta"=>"",
            "RUT_DvConsulta"=>"",
            "irAccionAtPublico"=>"Consulta",
            "NOM_Consulta"=>"",
            "APE_Paterno"=>"",
            "APE_Materno"=>"",
            "GLS_Razon"=>"",
            "COD_Tribunal"=>trim($tag2Hijo->getAttribute("value")),
            "COD_TribunalSinTodos"=>trim($tag2Hijo->getAttribute("value"))
          );


        }

        // sleep(15);
      }
    }
  }

  // unlink($cookie);
?>
