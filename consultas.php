<?php
	 // ini_set('display_errors', 'On');
	require('conexion.php');
	//select
	function ingresaTr($folio,$tribunal){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_ccpj.CLPJ_TRIBUNAL
(FOLIO, NOMBRE)
VALUES('{$folio}', '{$tribunal}')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				// $con->query("ROLLBACK");
				return $con->error;
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error_2";
		}
	}

	function ingresaCL($rit,$car,$fecha_ingreso,$ruc,$proc,$forma_inicio,$est_admin,$etapa,$est_proc,$tribunal){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_ccpj.CLPJ
(RIT, CARATULADO, FECHA_INGRESO, RUC, PROC, FORMA_INICIO, EST_ADMIN, ETAPA, EST_PROC, TRIBUNAL, FECHAHORA)
VALUES('{$rit}', '{$car}', '{$fecha_ingreso}', '{$ruc}', '{$proc}', '{$forma_inicio}', '{$est_admin}', '$etapa', '{$est_proc}', '{$tribunal}',NOW())";
			if ($con->query($sql)) {
			    return $con;
			}
			else{
				// $con->query("ROLLBACK");
				return $con->error;
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error_2";
		}
	}

	function ingresaCL_Lit($ruc,$ad,$sujeto,$rut,$persona,$nom_raz,$rit,$tribunal,$id){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_ccpj.CLPJ_LIT
(RUC, AD, SUJETO, RUT, PERSONA, NOM_RAZ, RIT, TRIBUNAL, IDCLPJ)
VALUES('{$ruc}', '{$ad}', '{$sujeto}', '{$rut}', '{$persona}', '{$nom_raz}', '{$rit}','{$tribunal}','{$id}')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				// $con->query("ROLLBACK");
				return $con->error;
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error_2";
		}
	}

	function ingresaCL_Mat($ruc,$codigo,$glosa,$rit,$tribunal,$id){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_ccpj.CLPJ_MAT
(RUC, CODIGO, GLOSA, RIT, TRIBUNAL, IDCLPJ)
VALUES('{$ruc}', '{$codigo}', '{$glosa}', '{$rit}','{$tribunal}','{$id}')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				// $con->query("ROLLBACK");
				return $con->error;
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error_2";
		}
	}

	function ingresaCL_Mov($ruc,$rit,$folio,$etapa,$tramite,$desc_tramite,$fecha,$estado,$tribunal,$id){
		$con = conectar();
		$con->query("START TRANSACTION");
		if($con != 'No conectado'){
			$sql = "INSERT INTO rriveros_ccpj.CLPJ_MOV
(RUC,RIT,FOLIO,ETAPA,TRAMITE,DESC_TRAMITE,FECHA,ESTADO,TRIBUNAL, IDCLPJ)
VALUES('{$ruc}', '{$rit}', '{$folio}', '{$etapa}', '{$tramite}', '{$desc_tramite}', '{$fecha}', '{$estado}','{$tribunal}','{$id}')";
			if ($con->query($sql)) {
				$con->query("COMMIT");
			    return "Ok";
			}
			else{
				// $con->query("ROLLBACK");
				return $con->error;
			}
		}
		else{
			$con->query("ROLLBACK");
			return "Error_2";
		}
	}

	//Para excel Ezentis
	function consultaExcelEzentis(){
		$con = conectar();
		if($con != 'No conectado'){
			$sql = "SELECT C.RUC, C.RIT, C.TRIBUNAL, C.FECHA_INGRESO, C.CARATULADO
FROM CLPJ C
LEFT JOIN CLPJ_LIT L
ON C.IDCLPJ = L.IDCLPJ
WHERE C.FECHA_INGRESO > DATE_SUB(NOW(),INTERVAL 30 DAY)
AND L.RUT IN
(
	'77552960-1',
	'96837950-K',
	'99548940-6'
)
ORDER BY C.FECHA_INGRESO DESC";
			if ($row = $con->query($sql)) {
				$return = array();
				while($array = $row->fetch_array(MYSQLI_BOTH)){
					$return[] = $array;
				}

				return $return;
			}
			else{
				return "Error";
			}
		}
		else{
			return "Error";
		}
	}

?>
